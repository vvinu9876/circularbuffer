try{
    function setAttribute(attr,val){
        $("#"+attr).html(val);
    }

    function getListHTML(val){
       return `<li class="list-group-item d-flex px-3">
                        <span class="text-semibold text-fiord-blue">`+val+`</span>
                </li>`;
    }

    function setElements(array_string){
        try{
            var array_list = JSON.parse(array_string);
            if(array_list.length==0){
                $("#element_content").html(getListHTML("No Elements"));
                return;
            }
            var html_list = "";
            array_list.forEach(function(elem,index){
                html_list = html_list + getListHTML((index+1)+". "+elem);
            })
            $("#element_content").html(html_list);
        }
        catch(err){
            $("#content").html(err.message);
        }
    }

    $("#addElement").click(function(){
        var value = $("#new_element").val();
        addBufferElement(value);
    });

    $("#pop").click(function(){
        popElement();
    })

}
catch(err){
    $("#content").html(err.message);
}


$(document).ready(function(){
    setAllAttributes();
    setBufferElements();
})