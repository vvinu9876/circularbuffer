# CIRCULAR BUFFERING

__Clone this repo to run the application!__

This is a minimal Ultralight app you can use with the [Writing Your First App](https://docs.ultralig.ht/docs/writing-your-first-app) article in the Ultralight documentation.

## LIBRARIES USED
### 1. [ULTRALIGHT-UX](https://docs.ultralig.ht/)
### 2. [SHARDS UI](https://designrevision.com/downloads/shards-dashboard-lite/)

## 1. Install the Prerequisites

Before you build and run, you'll need to [install the prerequisites](https://docs.ultralig.ht/docs/installing-prerequisites) for your platform.

## 2. Clone and build the app

To clone the repo and build, run the following:

```shell
git clone git clone https://vvinu9876@bitbucket.org/vvinu9876/circularbuffer.git
cd circularbuffer
mkdir build
cd build
cmake ..
cmake --build . --config Release
```

> **Note**: _To force CMake to generate 64-bit projects on Windows, use `cmake .. -DCMAKE_GENERATOR_PLATFORM=x64` instead of `cmake ..`_

## 3. Run the app

### On macOS and Linux

Navigate to `circularbuffer/build` and run `MyApp` to launch the program.

### On Windows

Navigate to `circularbuffer/build/Release` and run `MyApp` to launch the program.

## Further Reading

[Circular Buffering](https://en.wikipedia.org/wiki/Circular_buffer) 
