#include<iostream> 
#include <JavaScriptCore/JavaScript.h>
#include "CircularBuffer.cpp"
#include<string>
#include<sstream>
#include<list>
#pragma once

using namespace std;
circular_buffer<string> circle(10); 

string JSStringToStdString(JSStringRef jsString) {
    size_t maxBufferSize = JSStringGetMaximumUTF8CStringSize(jsString);
    char* utf8Buffer = new char[maxBufferSize];
    size_t bytesWritten = JSStringGetUTF8CString(jsString, utf8Buffer, maxBufferSize);
    std::string utf_string = std::string(utf8Buffer, bytesWritten -1);
    delete [] utf8Buffer;
    return utf_string;
}

void modifyHTML(JSContextRef ctx,const char* textscript){
    JSStringRef script = JSStringCreateWithUTF8CString(textscript);
    JSEvaluateScript(ctx, script, 0, 0, 0, 0);
    JSStringRelease(script);
}



string toString(size_t val){
    stringstream str;
    str<<val;
    return str.str();
}

string getExecString(string attr,string val){
    return "setAttribute(\"" +attr+ "\" , \""+ val + "\");";
}


void setValues(JSContextRef ctx){
    string to_set_current_size = getExecString("size",toString(circle.size()));
    cout<<to_set_current_size<<endl;
    string to_set_capcity = getExecString("capacity",toString(circle.capacity()));
    modifyHTML(ctx,to_set_capcity.c_str());
    cout.flush();
    modifyHTML(ctx,to_set_current_size.c_str());
    string to_set_is_full = getExecString("isFull",(circle.full()?"TRUE":"FALSE"));
    modifyHTML(ctx,to_set_is_full.c_str());
    string to_set_is_empty = getExecString("isEmpty",(circle.empty()?"TRUE":"FALSE"));
    modifyHTML(ctx,to_set_is_empty.c_str());
}

void setAllBufferElements(JSContextRef ctx){
        list<string> values;
        //get all values
        while(!circle.empty()){
            values.push_back(circle.get());
        }
        //lets make string  
        string json_array = "[ ";
        int n = values.size();
        int i=0;
        //make string and put back values
        for(auto x : values){
            cout<<"value = "<<x<<endl;
            cout.flush();
            json_array = json_array + "\""+x+"\""; 
            if(i!=n-1) //add comma (donot add comma after  last element )
                json_array = json_array + ",";
            i++;
            //put back the value
            circle.put(x);
        }
        json_array = json_array + "]";
        string to_set_values = " setElements('"+json_array+"');";
        cout<< to_set_values <<endl;
        cout.flush();
        modifyHTML(ctx,to_set_values.c_str());
}

JSValueRef setAllAttributes(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
    setValues(ctx);
    cout<<"We receieved A Call"<<endl;
    cout.flush();
    return JSValueMakeNull(ctx);
}

JSValueRef setBufferElements(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
        setAllBufferElements(ctx);
        return JSValueMakeNull(ctx);
}


JSValueRef addBufferElement(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
    string new_element = JSStringToStdString(JSValueToStringCopy(ctx,arguments[0],exception));
    circle.put(new_element);
    cout<<"Inserted "<<new_element<<endl;
    setValues(ctx);
    setAllBufferElements(ctx);
    return JSValueMakeNull(ctx);
}

JSValueRef popElement(JSContextRef ctx, JSObjectRef function, JSObjectRef thisObject, size_t argumentCount, 
                        const JSValueRef arguments[], JSValueRef* exception){
    string poped_element = circle.get();
    cout<<"Poped Element = "<<poped_element<<endl;
    cout.flush();
    setValues(ctx);
    setAllBufferElements(ctx);
    return JSValueMakeNull(ctx);
}



                            






